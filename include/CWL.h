#ifndef CWL_H
#define CWL_H

#ifndef CWL_DECLARE
#	ifdef CWL_STATIC
#		ifdef __cplusplus
#			define CWL_DECLARE extern "C" static
#		else
#			define CWL_DECLARE static
#		endif
#	else
#		ifdef __cplusplus
#			define CWL_DECLARE extern "C" extern
#		else
#			define CWL_DECLARE extern
#		endif
#	endif
#endif /* CWL_DECLARE */

#ifndef CWL_RESTRICT
#	if !defined(__cplusplus) && defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
#		define CWL_RESTRICT restrict
#	elif defined(__GNUC__)
#		define CWL_RESTRICT __restrict__
#	elif defined(_MSC_VER)
#		define CWL_RESTRICT __restrict
#	else
#		define CWL_RESTRICT
#	endif
#endif /* CWL_RESTRICT */

#ifndef CWL_INLINE
#	if defined(__cplusplus) || (defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L)
#		define CWL_INLINE inline
#	elif defined(__GNUC__)
#		define CWL_INLINE __inline__
#	elif defined(_MSC_VER)
#		define CWL_INLINE __inline
#	else
#		define CWL_INLINE
#	endif
#endif /* CWL_INLINE */

#ifndef NULL
#	ifdef __cplusplus
#		define NULL (0)
#	else
#		define NULL ((void *)0)
#	endif
#endif /* NULL */

#ifndef CWL_SIZE
#	define CWL_SIZE unsigned long
#endif /* CWL_SIZE */

#endif /* CWL_H */
