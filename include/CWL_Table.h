#ifndef CWL_TABLE_H
#define CWL_TABLE_H

#include "CWL_Allocator.h"

typedef struct CWL_Table CWL_Table;

typedef CWL_SIZE (*CWL_Table_Hasher)(const void *);
typedef int      (*CWL_Table_Compare)(const void *, const void *);
typedef void     (*CWL_Table_Destroyer)(void *);

CWL_DECLARE int CWL_Table_create(CWL_Table **, const CWL_Allocator *, CWL_Table_Hasher, CWL_Table_Compare, CWL_Table_Destroyer,
                                 CWL_Table_Destroyer, CWL_SIZE, double);
CWL_DECLARE int CWL_Table_destroy(CWL_Table **);
CWL_DECLARE int CWL_Table_set(CWL_Table *, void *, void *);
CWL_DECLARE int CWL_Table_get(CWL_Table *, const void *, void **);

#endif /* CWL_TABLE_H */

#ifdef CWL_IMPLEMENTATION

#include "CWL_Error.h"

typedef struct CWL_Table__Bucket CWL_Table__Bucket;

struct CWL_Table__Bucket {
	void *_key, *_value;
	CWL_Table__Bucket *_next;
};

struct CWL_Table {
	CWL_Allocator _allocator;
	CWL_Table_Hasher _hasher;
	CWL_Table_Compare _compare;
	CWL_Table_Destroyer _key_destroyer, _value_destroyer;
	CWL_Table__Bucket **_buckets;
	CWL_SIZE _capacity, _count;
	double _load_factor;
};

static int
CWL_Table__rehash(CWL_Table *const table)
{
	CWL_Table__Bucket **iter, **end, **new_buckets, *bucket, *next, *new_bucket;
	CWL_SIZE new_capacity, new_size, hash;
	if ((double)table->_count / (double)table->_capacity < table->_load_factor)
		return CWL_ERROR_NONE;
	new_capacity = table->_capacity * table->_capacity;
	if (new_capacity < table->_capacity)
		return -CWL_ERROR_OVERFLOW;
	new_size = sizeof(*new_buckets) * new_capacity;
	if (new_size < sizeof(*new_buckets) * table->_capacity) {
		return -CWL_ERROR_OVERFLOW;
	}
	if ((new_buckets = table->_allocator.allocate(new_size)) == NULL)
		return -CWL_ERROR_OVERFLOW;
	for (iter = new_buckets, end = new_buckets + new_capacity; iter != end; ++iter)
		*iter = NULL;
	for (iter = table->_buckets, end = table->_buckets + table->_capacity; iter != end; ++iter) {
		for (bucket = *iter; bucket != NULL;) {
			hash = table->_hasher(bucket->_key) % new_capacity;
			if (new_buckets[hash] == NULL) {
				next = bucket->_next;
				bucket->_next = NULL;
				new_buckets[hash] = bucket;
				bucket = next;
				continue;
			}
			for (new_bucket = new_buckets[hash]; new_bucket->_next != NULL; new_bucket = new_bucket->_next)
				;
			next = bucket->_next;
			bucket->_next = NULL;
			new_bucket->_next = bucket;
			bucket = next;
		}
	}
	table->_allocator.deallocate(table->_buckets);
	table->_buckets = new_buckets;
	table->_capacity = new_capacity;
	return CWL_ERROR_NONE;
}

CWL_DECLARE int
CWL_Table_create(CWL_Table **const table, const CWL_Allocator *const allocator, const CWL_Table_Hasher hasher,
                 const CWL_Table_Compare compare, const CWL_Table_Destroyer key_destroyer,
                 const CWL_Table_Destroyer value_destroyer, const CWL_SIZE capacity, const double load_factor)
{
	CWL_Table__Bucket **iter, **end;
	if (table == NULL || allocator == NULL || hasher == NULL || compare == NULL || key_destroyer == NULL ||
	    value_destroyer == NULL || capacity < 2 || !(load_factor > 0.0 && load_factor < 1.0))
		return -CWL_ERROR_ARGUMENT;
	if ((*table = allocator->allocate(sizeof(**table))) == NULL)
		return -CWL_ERROR_MEMORY;
	if (((*table)->_buckets = allocator->allocate(sizeof(*(*table)->_buckets) * capacity)) == NULL) {
		allocator->deallocate(*table);
		return -CWL_ERROR_MEMORY;
	}
	for (iter = (*table)->_buckets, end = (*table)->_buckets + capacity; iter != end; ++iter)
		*iter = NULL;
	(*table)->_allocator = *allocator;
	(*table)->_hasher = hasher;
	(*table)->_compare = compare;
	(*table)->_key_destroyer = key_destroyer;
	(*table)->_value_destroyer = value_destroyer;
	(*table)->_capacity = capacity;
	(*table)->_load_factor = load_factor;
	(*table)->_count = 0;
	return CWL_ERROR_NONE;
}

CWL_DECLARE int
CWL_Table_destroy(CWL_Table **const table)
{
	CWL_Table__Bucket **iter, **end, *bucket, *next;
	if (table == NULL || *table == NULL)
		return -CWL_ERROR_ARGUMENT;
	for (iter = (*table)->_buckets, end = (*table)->_buckets + (*table)->_capacity; iter != end; ++iter) {
		for (bucket = *iter; bucket != NULL;) {
			next = bucket->_next;
			(*table)->_key_destroyer(bucket->_key);
			(*table)->_value_destroyer(bucket->_value);
			(*table)->_allocator.deallocate(bucket);
			bucket = next;
		}
	}
	(*table)->_allocator.deallocate((*table)->_buckets);
	(*table)->_allocator.deallocate(*table);
	*table = NULL;
	return CWL_ERROR_NONE;
}

CWL_DECLARE int
CWL_Table_set(CWL_Table *const table, void *key, void *value)
{
	CWL_Table__Bucket *bucket;
	CWL_SIZE hash;
	if (table == NULL || key == NULL || value == NULL)
		return -CWL_ERROR_ARGUMENT;
	hash = table->_hasher(key) % table->_capacity;
	if (table->_buckets[hash] == NULL) {
		if ((table->_buckets[hash] = table->_allocator.allocate(sizeof(*table->_buckets[hash]))) == NULL)
			return -CWL_ERROR_MEMORY;
		table->_buckets[hash]->_key = key;
		table->_buckets[hash]->_value = value;
		table->_buckets[hash]->_next = NULL;
		++table->_count;
		return CWL_Table__rehash(table);
	}
	bucket = table->_buckets[hash];
	if (table->_compare(bucket->_key, key) == 0) {
		table->_key_destroyer(bucket->_key);
		bucket->_key = key;
		table->_value_destroyer(bucket->_value);
		bucket->_value = value;
		return CWL_ERROR_NONE;
	}
	for (; bucket->_next != NULL; bucket = bucket->_next) {
		if (table->_compare(bucket->_next->_key, key) == 0) {
			table->_key_destroyer(bucket->_next->_key);
			bucket->_next->_key = key;
			table->_value_destroyer(bucket->_next->_value);
			bucket->_next->_value = value;
			return CWL_ERROR_NONE;
		}
	}
	if ((bucket->_next = table->_allocator.allocate(sizeof(*bucket->_next))) == NULL)
		return -CWL_ERROR_MEMORY;
	bucket->_next->_key = key;
	bucket->_next->_value = value;
	bucket->_next->_next = NULL;
	++table->_count;
	return CWL_Table__rehash(table);
}

CWL_DECLARE int
CWL_Table_get(CWL_Table *const table, const void *const key, void **const value)
{
	CWL_Table__Bucket *bucket;
	if (table == NULL || key == NULL || value == NULL)
		return -CWL_ERROR_ARGUMENT;
	if (table->count == 0)
		return 0;
	bucket = table->_buckets[table->_hasher(key) % table->_capacity];
	for (; bucket != NULL; bucket = bucket->_next) {
		if (table->_compare(key, bucket->_key) == 0) {
			*value = bucket->_value;
			return 1;
		}
	}
	return 0;
}

#endif /* CWL_IMPLEMENTATION */
