#ifndef CWL_ALLOCATOR_H
#define CWL_ALLOCATOR_H

#include "CWL.h"

typedef struct CWL_Allocator {
	void *(*allocate)(CWL_SIZE);
	void *(*reallocate)(void *, CWL_SIZE);
	void  (*deallocate)(void *);
} CWL_Allocator;

#endif /* CWL_ALLOCATOR_H */
