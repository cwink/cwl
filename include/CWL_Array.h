#ifndef CWL_ARRAY_H
#define CWL_ARRAY_H

#include "CWL_Allocator.h"

typedef struct CWL_Array CWL_Array;

typedef void (*CWL_Array_Destroyer)(void *);

CWL_DECLARE int CWL_Array_create(CWL_Array **, const CWL_Allocator *, CWL_Array_Destroyer, CWL_SIZE);
CWL_DECLARE int CWL_Array_destroy(CWL_Array **);
CWL_DECLARE int CWL_Array_insert(CWL_Array *, void *, CWL_SIZE);
CWL_DECLARE int CWL_Array_erase(CWL_Array *, CWL_SIZE);
CWL_DECLARE int CWL_Array_get(CWL_Array *, void **, CWL_SIZE);
CWL_DECLARE int CWL_Array_length(const CWL_Array *, CWL_SIZE *);

#endif /* CWL_ARRAY_H */

#ifdef CWL_IMPLEMENTATION

#include "CWL_Error.h"

struct CWL_Array {
	CWL_Allocator _allocator;
	void **_data;
	CWL_Array_Destroyer _destroyer;
	CWL_SIZE _capacity, _length;
};

static int
CWL_Array__resize(CWL_Array *const array)
{
	void **new_data;
	CWL_SIZE new_capacity, new_size;
	if (array->_length != array->_capacity)
		return CWL_ERROR_NONE;
	new_capacity = array->_capacity * array->_capacity;
	if (new_capacity < array->_capacity)
		return -CWL_ERROR_OVERFLOW;
	new_size = sizeof(*array->_data) * new_capacity;
	if (new_size < sizeof(*array->_data) * array->_capacity)
		return -CWL_ERROR_OVERFLOW;
	if ((new_data = array->_allocator.reallocate(array->_data, new_size)) == NULL)
		return -CWL_ERROR_MEMORY;
	array->_data = new_data;
	array->_capacity = new_capacity;
	return CWL_ERROR_NONE;
}

CWL_DECLARE int
CWL_Array_create(CWL_Array **const array, const CWL_Allocator *const allocator,
                 const CWL_Array_Destroyer destroyer, const CWL_SIZE capacity)
{
	if (array == NULL || allocator == NULL || destroyer == NULL || capacity < 2)
		return -CWL_ERROR_ARGUMENT;
	if ((*array = allocator->allocate(sizeof(**array))) == NULL)
		return -CWL_ERROR_MEMORY;
	if (((*array)->_data = allocator->allocate(sizeof(*((*array)->_data)) * capacity)) == NULL) {
		allocator->deallocate(*array);
		return -CWL_ERROR_MEMORY;
	}
	(*array)->_allocator = *allocator;
	(*array)->_destroyer = destroyer;
	(*array)->_capacity = capacity;
	(*array)->_length = 0;
	return CWL_ERROR_NONE;
}

CWL_DECLARE int
CWL_Array_destroy(CWL_Array **const array)
{
	void **iter, **end;
	if (array == NULL || *array == NULL)
		return -CWL_ERROR_ARGUMENT;
	for (iter = (*array)->_data, end = (*array)->_data + (*array)->_length; iter != end; ++iter)
		(*array)->_allocator.deallocate(*iter);
	(*array)->_allocator.deallocate((*array)->_data);
	(*array)->_allocator.deallocate(*array);
	*array = NULL;
	return CWL_ERROR_NONE;
}

CWL_DECLARE int
CWL_Array_insert(CWL_Array *const array, void *value, const CWL_SIZE index)
{
	void **iter, **begin;
	int error;
	if (array == NULL || value == NULL || index > array->_length)
		return -CWL_ERROR_ARGUMENT;
	if (array->_length + 1 < array->_length)
		return -CWL_ERROR_OVERFLOW;
	if ((error = CWL_Array__resize(array)) < 0)
		return error;
	for (iter = array->_data + array->_length, begin = array->_data + index; iter != begin; --iter)
		*iter = *(iter - 1);
	*iter = value;
	++array->_length;
	return CWL_ERROR_NONE;
}

CWL_DECLARE int
CWL_Array_erase(CWL_Array *const array, const CWL_SIZE index)
{
	void **iter, **end;
	if (array == NULL || index >= array->_length)
		return -CWL_ERROR_ARGUMENT;
	array->_allocator.deallocate(*(iter = array->_data + index));
	for (end = array->_data + array->_length; iter != end; ++iter)
		*iter = *(iter + 1);
	--array->_length;
	return CWL_ERROR_NONE;
}

CWL_DECLARE int
CWL_Array_get(CWL_Array *const array, void **value, const CWL_SIZE index)
{
	if (array == NULL || value == NULL || index >= array->_length)
		return -CWL_ERROR_ARGUMENT;
	*value = array->_data[index];
	return CWL_ERROR_NONE;
}

CWL_DECLARE int
CWL_Array_length(const CWL_Array *const array, CWL_SIZE *const length)
{
	if (array == NULL || length == NULL)
		return -CWL_ERROR_ARGUMENT;
	*length = array->_length;
	return CWL_ERROR_NONE;
}

#endif /* CWL_IMPLEMENTATION */
