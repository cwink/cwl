#ifndef CWL_ERROR_H
#define CWL_ERROR_H

#define CWL_ERROR_NONE     0
#define CWL_ERROR_ARGUMENT 1
#define CWL_ERROR_MEMORY   2
#define CWL_ERROR_OVERFLOW 3

#endif /* CWL_ERROR_H */
