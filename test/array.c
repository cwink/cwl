#include "test.h"

#include "../include/CWL_Array.h"

#define CWL_IMPLEMENTATION
#include "../include/CWL_Array.h"

static int *
int_duplicate(const int value)
{
	int *new_value;
	new_value = malloc(sizeof(*new_value));
	*new_value = value;
	return new_value;
}

TEST_CREATE(array_insert_begin)
{
	CWL_Allocator allocator;
	CWL_Array *array;
	void *value;
	CWL_SIZE length;
	allocator.allocate = malloc;
	allocator.reallocate = realloc;
	allocator.deallocate = free;
	TEST_ASSERT(CWL_Array_create(NULL, &allocator, free, 2)   == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_create(&array, NULL, free, 2)       == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_create(&array, &allocator, NULL, 2) == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_create(&array, &allocator, free, 1) == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_create(&array, &allocator, free, 2) ==  CWL_ERROR_NONE);
	TEST_ASSERT(CWL_Array_length(NULL, &length)   == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_length(array, NULL)     == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_length(array, &length)  ==  CWL_ERROR_NONE && length == 0);
	value = int_duplicate(-51);
	TEST_ASSERT(CWL_Array_insert(NULL, value, 0)                == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_insert(array, NULL, 0)                == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_insert(array, value, 1)               == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_insert(array, value, 0)               ==  CWL_ERROR_NONE);
	TEST_ASSERT(CWL_Array_insert(array, int_duplicate(72), 0)   ==  CWL_ERROR_NONE);
	TEST_ASSERT(CWL_Array_insert(array, int_duplicate(687), 0)  ==  CWL_ERROR_NONE);
	TEST_ASSERT(CWL_Array_insert(array, int_duplicate(-27), 0)  ==  CWL_ERROR_NONE);
	TEST_ASSERT(CWL_Array_insert(array, int_duplicate(1817), 0) ==  CWL_ERROR_NONE);
	TEST_ASSERT(CWL_Array_length(array, &length)                ==  CWL_ERROR_NONE && length == 5);
	TEST_ASSERT(CWL_Array_get(NULL, &value, 0)  == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_get(array, NULL, 0)   == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_get(array, &value, 5) == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_get(array, &value, 0) ==  CWL_ERROR_NONE && *(int *)value ==  1817);
	TEST_ASSERT(CWL_Array_get(array, &value, 1) ==  CWL_ERROR_NONE && *(int *)value == -27);
	TEST_ASSERT(CWL_Array_get(array, &value, 2) ==  CWL_ERROR_NONE && *(int *)value ==  687);
	TEST_ASSERT(CWL_Array_get(array, &value, 3) ==  CWL_ERROR_NONE && *(int *)value ==  72);
	TEST_ASSERT(CWL_Array_get(array, &value, 4) ==  CWL_ERROR_NONE && *(int *)value == -51);
	TEST_ASSERT(CWL_Array_erase(NULL, 0)  == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_erase(array, 5) == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_erase(array, 0) ==  CWL_ERROR_NONE);
	TEST_ASSERT(CWL_Array_erase(array, 3) ==  CWL_ERROR_NONE);
	TEST_ASSERT(CWL_Array_erase(array, 1) ==  CWL_ERROR_NONE);
	TEST_ASSERT(CWL_Array_get(array, &value, 0) ==  CWL_ERROR_NONE && *(int *)value == -27);
	TEST_ASSERT(CWL_Array_get(array, &value, 1) ==  CWL_ERROR_NONE && *(int *)value ==  72);
	TEST_ASSERT(CWL_Array_destroy(NULL)   == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_destroy(&array) ==  CWL_ERROR_NONE && array == NULL);
}

TEST_CREATE(array_insert_end)
{
	CWL_Allocator allocator;
	CWL_Array *array;
	void *value;
	CWL_SIZE length;
	allocator.allocate = malloc;
	allocator.reallocate = realloc;
	allocator.deallocate = free;
	TEST_ASSERT(CWL_Array_create(NULL, &allocator, free, 2)   == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_create(&array, NULL, free, 2)       == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_create(&array, &allocator, NULL, 2) == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_create(&array, &allocator, free, 1) == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_create(&array, &allocator, free, 2) ==  CWL_ERROR_NONE);
	TEST_ASSERT(CWL_Array_length(NULL, &length)   == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_length(array, NULL)     == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_length(array, &length)  ==  CWL_ERROR_NONE && length == 0);
	value = int_duplicate(-51);
	TEST_ASSERT(CWL_Array_insert(NULL, value, 0)                == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_insert(array, NULL, 0)                == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_insert(array, value, 1)               == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_insert(array, value, 0)               ==  CWL_ERROR_NONE);
	TEST_ASSERT(CWL_Array_insert(array, int_duplicate(72), 1)   ==  CWL_ERROR_NONE);
	TEST_ASSERT(CWL_Array_insert(array, int_duplicate(687), 2)  ==  CWL_ERROR_NONE);
	TEST_ASSERT(CWL_Array_insert(array, int_duplicate(-27), 3)  ==  CWL_ERROR_NONE);
	TEST_ASSERT(CWL_Array_insert(array, int_duplicate(1817), 4) ==  CWL_ERROR_NONE);
	TEST_ASSERT(CWL_Array_length(array, &length)                ==  CWL_ERROR_NONE && length == 5);
	TEST_ASSERT(CWL_Array_get(NULL, &value, 0)  == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_get(array, NULL, 0)   == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_get(array, &value, 5) == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_get(array, &value, 0) ==  CWL_ERROR_NONE && *(int *)value == -51);
	TEST_ASSERT(CWL_Array_get(array, &value, 1) ==  CWL_ERROR_NONE && *(int *)value ==  72);
	TEST_ASSERT(CWL_Array_get(array, &value, 2) ==  CWL_ERROR_NONE && *(int *)value ==  687);
	TEST_ASSERT(CWL_Array_get(array, &value, 3) ==  CWL_ERROR_NONE && *(int *)value == -27);
	TEST_ASSERT(CWL_Array_get(array, &value, 4) ==  CWL_ERROR_NONE && *(int *)value ==  1817);
	TEST_ASSERT(CWL_Array_erase(NULL, 0)  == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_erase(array, 5) == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_erase(array, 0) ==  CWL_ERROR_NONE);
	TEST_ASSERT(CWL_Array_erase(array, 3) ==  CWL_ERROR_NONE);
	TEST_ASSERT(CWL_Array_erase(array, 1) ==  CWL_ERROR_NONE);
	TEST_ASSERT(CWL_Array_get(array, &value, 0) ==  CWL_ERROR_NONE && *(int *)value ==  72);
	TEST_ASSERT(CWL_Array_get(array, &value, 1) ==  CWL_ERROR_NONE && *(int *)value == -27);
	TEST_ASSERT(CWL_Array_destroy(NULL)   == -CWL_ERROR_ARGUMENT);
	TEST_ASSERT(CWL_Array_destroy(&array) ==  CWL_ERROR_NONE && array == NULL);
}

TEST_MAIN()
{
	TEST_INITIALIZE(array);
	TEST_CALL(array_insert_begin);
	TEST_CALL(array_insert_end);
	TEST_TERMINATE();
}
