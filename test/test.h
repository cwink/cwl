#ifndef TEST_H
#define TEST_H

#include "../include/CWL.h"

#include <stdio.h>
#include <stdlib.h>

#define TEST_MAIN()                                                           \
	const char *                                                          \
	__asan_default_options()                                              \
	{                                                                     \
		return "verbosity=1:detect_invalid_pointer_pairs=2";          \
	}                                                                     \
	                                                                      \
	const char *                                                          \
	__ubsan_default_options()                                             \
	{                                                                     \
		return "print_stacktrace=1";                                  \
	}                                                                     \
	                                                                      \
	int                                                                   \
	main(void)

#define TEST_INITIALIZE(NAME)                                                 \
	const char *test_name;                                                \
	size_t test_passes, test_failures;                                    \
	test_name = #NAME;                                                    \
	test_passes = 0;                                                      \
	test_failures = 0;                                                    \
	fprintf(stdout, "Running test module '%s'...\n", test_name)

#define TEST_TERMINATE()                                                      \
	fprintf(stdout, "Finished test module '%s' with %lu passes and %lu "  \
	                "failures.\n", test_name, test_passes,                \
	                test_failures);                                       \
	return test_failures == 0 ? EXIT_SUCCESS : EXIT_FAILURE

#define TEST_CREATE(NAME)                                                     \
	static void NAME(size_t *const CWL_RESTRICT test_passes,              \
	                 size_t *const CWL_RESTRICT test_failures)

#define TEST_CALL(NAME)                                                       \
	fprintf(stdout, "\tRunning test '%s'...\n", #NAME);                   \
	NAME(&test_passes, &test_failures);                                   \
	fprintf(stdout, "\tFinished running test '%s'.\n", #NAME);

#define TEST_ASSERT(CONDITION)                                                \
	do {                                                                  \
		if (CONDITION) {                                              \
			fprintf(stdout, "\t\t[%u] PASS: %s\n", __LINE__,      \
			                #CONDITION);                          \
			++*test_passes;                                       \
			break;                                                \
		}                                                             \
		fprintf(stderr, "\t\t[%u] FAIL: %s\n", __LINE__, #CONDITION); \
		++*test_failures;                                             \
	} while(0)

#endif /* TEST_H */
