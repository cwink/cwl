#!/bin/bash

TEST=`echo $1 | tr -d '[:space:]'`
COMMAND=`echo $2 | tr -d '[:space:]' | tr '[:upper:]' '[:lower:]'`

if [ "${TEST}" == "" ]; then
	echo -en "Usage: ./run [test] [command]\n\n"
	echo -en "\tRuns the provided test, i.e. 'array'. If no command is\n"
	echo -en "\tgiven then profile, coverage, sanitize, and clean\n"
	echo -en "\tare ran.\n\n"
	echo -en "\tcommands:\n"
	echo -en "\t\tprofile  - Profiles the test.\n"
	echo -en "\t\tcoverage - Runs test coverages against the test.\n"
	echo -en "\t\tsanitize - Runs sanitizers against the test.\n"
	echo -en "\t\trun      - Runs the test.\n"
	echo -en "\t\tdebug    - Debugs the test.\n"
	echo -en "\t\tclean    - Cleans the test directory.\n"
	exit 0
fi

if [ ! -e "${TEST}.c" ]; then
	echo "Module '${TEST}' does not exist." 1>&2 >&2
	exit 1
fi

test_profile()
{
	echo "================================================================"
	echo -en "\tProfiling ${TEST}...\n"
	echo "================================================================"
	make EXTRA_CFLAGS="-pg" clean "${TEST}" || return $?
	./"${TEST}"                             || return $?
	gprof -b "${TEST}" gmon.out             || return $?
	rm -rf gmon.out
	return 0
}

test_coverage()
{
	echo "================================================================"
	echo -en "\tCoveraging ${TEST}...\n"
	echo "================================================================"
	make EXTRA_CFLAGS="--coverage" LD_FLAGS="--coverage"                  \
	     clean "${TEST}" || return $?
	./"${TEST}"          || return $?
	gcov -jkt "${TEST}"  || return $?
	rm -rf "${TEST}.gcno" "${TEST}.gcda"
	return 0
}

test_sanitize_asan()
{
	echo "================================================================"
	echo -en "\tAddress sanitizing ${TEST}...\n"
	echo "================================================================"
	local ASAN_FLAGS='-fsanitize=address,pointer-compare,pointer-subtract'
	local ASAN_FLAGS="${ASAN_FLAGS},leak -fno-omit-frame-pointer -g"
	make EXTRA_CFLAGS="${ASAN_FLAGS}" clean "${TEST}" || return $?
	./"${TEST}"                                       || return $?
}

test_sanitize_ubsan()
{
	echo "================================================================"
	echo -en "\tUndefined behavior sanitizing ${TEST}...\n"
	echo "================================================================"
	UBSAN_FLAGS='-fsanitize=undefined -fno-omit-frame-pointer -g'
	make EXTRA_CFLAGS="${UBSAN_FLAGS}" clean "${TEST}" || return $?
	./"${TEST}"                                        || return $?
}

test_run()
{
	echo "================================================================"
	echo -en "\tRunning ${TEST}...\n"
	echo "================================================================"
	make "${TEST}" || return $?
	./"${TEST}"    || return $?
}

test_debug()
{
	echo "================================================================"
	echo -en "\tDebugging ${TEST}...\n"
	echo "================================================================"
	make EXTRA_CFLAGS="-g -Og" "${TEST}" || return $?
	gdb ./"${TEST}"                      || return $?
}

test_clean()
{
	make clean
}

case "${COMMAND}" in
	'profile')
		test_profile  || exit $?
		;;
	'coverage')
		test_coverage || exit $?
		;;
	'sanitize')
		test_sanitize_asan  || exit $?
		test_sanitize_ubsan || exit $?
		;;
	'run')
		test_run || exit $?
		;;
	'debug')
		test_debug || exit $?
		;;
	'clean')
		test_clean || exit $?
		;;
	*)
		test_profile        || exit $?
		test_coverage       || exit $?
		test_sanitize_asan  || exit $?
		test_sanitize_ubsan || exit $?
		test_clean          || exit $?
		;;
esac

exit 0
