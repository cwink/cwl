#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/CWL_Table.h"

#define CWL_IMPLEMENTATION
#include "../include/CWL_Table.h"

static CWL_SIZE
string_hasher(const void *const data)
{
	const char *bytes;
	CWL_SIZE hash;
	hash = 0xcbf29ce484222325;
	for (bytes = data; *bytes != '\0'; ++bytes) {
		hash ^= *bytes;
		hash *= 0x100000001b3;
	}
	return hash;
}

static int
string_compare(const void *left, const void *right)
{
	return strcmp(left, right);
}

static char *
string_duplicate(const char *const string)
{
	char *new_string;
	new_string = malloc(sizeof(char) * (strlen(string) + 1));
	strcpy(new_string, string);
	return new_string;
}

int
main(void)
{
	CWL_Allocator allocator;
	CWL_Table *table;
	void *value;
	allocator.allocate = malloc;
	allocator.reallocate = realloc;
	allocator.deallocate = free;
	CWL_Table_create(&table, &allocator, string_hasher, string_compare, free, free, 2, 0.75);
	CWL_Table_set(table, string_duplicate("name"), string_duplicate("John Doe"));
	CWL_Table_set(table, string_duplicate("organization"), string_duplicate("Acme Widgets Inc."));
	CWL_Table_set(table, string_duplicate("server"), string_duplicate("192.0.2.62"));
	CWL_Table_set(table, string_duplicate("port"), string_duplicate("143"));
	CWL_Table_set(table, string_duplicate("file"), string_duplicate("\"payroll.dat\""));
	if (CWL_Table_get(table, "name", &value))
		printf("|%s| -> |%s|\n", "name", (char *)value);
	CWL_Table_destroy(&table);
	return EXIT_SUCCESS;
}
